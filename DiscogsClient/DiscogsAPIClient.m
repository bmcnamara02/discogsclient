//
//  DiscogsAPIClient.m
//  DiscogsClient
//
//  Created by Barry McNamara on 12/07/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import "DiscogsAPIClient.h"

@implementation DiscogsAPIClient

+ (DiscogsAPIClient *)sharedClientInitWithToken:(NSString *)token {
    static DiscogsAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSString *tokenString = [NSString stringWithFormat:@"Authorization: Discogs token=%@", token];
    
//    [sessionConfig setHTTPAdditionalHeaders:@{@"Authorization": tokenString}];
//    NSLog(@"%@", sessionConfig.HTTPAdditionalHeaders);
    
    dispatch_once(&onceToken, ^{
        NSURL *baseURL = [NSURL URLWithString:@"https://api.discogs.com/"];
        _sharedClient = [[DiscogsAPIClient alloc] initWithBaseURL:baseURL
                                             sessionConfiguration:sessionConfig];
        _sharedClient.responseSerializer = [AFJSONResponseSerializer serializer];
    });
    
    return _sharedClient;
}

- (NSURLSessionDataTask *)searchForTerm:(NSString *)term
                             completion:( void (^)(NSArray *results, NSError *error) )completion {
    
    NSString *searchString = [NSString stringWithFormat:@"database/search?"];
    
    searchString = [searchString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionDataTask *task = [self GET:searchString
                                parameters:@{@"q": term}
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                       if (httpResponse.statusCode == 200) {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               completion(responseObject[@"folders"], nil);
                                           });
                                       } else {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               completion(nil, nil);
                                           });
                                           NSLog(@"Received: %@", responseObject);
                                           NSLog(@"Received HTTP %ld", (long)httpResponse.statusCode);
                                       }
                                       
                                   } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           completion(nil, error);
                                       });
                                   }];
    return task;

}

- (NSURLSessionDataTask *)getWishlistCompletion:( void (^)(NSArray *results, NSError *error) )completion {
    
    NSString *searchString = [NSString stringWithFormat:@"users/mrknott/collection/folders"];
    
    searchString = [searchString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionDataTask *task = [self GET:searchString
                                parameters:nil
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                       if (httpResponse.statusCode == 200) {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               completion(responseObject[@"folders"], nil);
                                               NSLog(@"%@", responseObject[@"folders"]);
                                           });
                                       } else {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               completion(nil, nil);
                                           });
                                           NSLog(@"Received: %@", responseObject);
                                           NSLog(@"Received HTTP %ld", (long)httpResponse.statusCode);
                                       }
                                       
                                   } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           completion(nil, error);
                                       });
                                   }];
    
    return task;

}

@end
