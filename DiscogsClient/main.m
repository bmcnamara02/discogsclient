//
//  main.m
//  DiscogsClient
//
//  Created by Barry McNamara on 11/07/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
