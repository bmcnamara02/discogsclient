//
//  MainViewController.m
//  DiscogsClient
//
//  Created by Barry McNamara on 12/07/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import "MainViewController.h"
#import "SearchTableViewController.h"
#import "DiscogsOAuthClient.h"
#import "UIKit+BBlock.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (id)init {
    self = [super init];
    
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserverForName:DiscogsClientDidLogInNotification
                                                          object:nil
                                                           queue:nil
                                                      usingBlock:^(NSNotification *note) {
            
            [self.navigationItem.rightBarButtonItem setTitle:NSLocalizedString(@"Log Out", nil)];
        }];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:DiscogsClientDidLogOutNotification
                                                          object:nil
                                                           queue:nil
                                                      usingBlock:^(NSNotification *note) {
            [self.navigationItem.rightBarButtonItem setTitle:NSLocalizedString(@"Log In", nil)];
        }];
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *logInOutString = ([[DiscogsOAuthClient sharedClient] isAuthorized]) ?
    NSLocalizedString(@"Log Out", nil) : NSLocalizedString(@"Log In", nil);
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:logInOutString
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(logInOut)];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Authorization
- (void)logInOut {
    if ([[DiscogsOAuthClient sharedClient] isAuthorized]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Are you sure you want to log out?", nil)
                                cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                           destructiveButtonTitle:NSLocalizedString(@"Log Out", nil)
                                 otherButtonTitle:nil
                                  completionBlock:^(NSInteger buttonIndex, UIActionSheet *actionSheet) {
                                      if (buttonIndex == actionSheet.destructiveButtonIndex) {
                                          [[DiscogsOAuthClient sharedClient] deauthorize];
                                          [self.navigationItem.rightBarButtonItem setTitle:NSLocalizedString(@"Log In", nil)];
                                      }
                                  }]
             showInView:self.view];

        });
    } else {
        [[DiscogsOAuthClient sharedClient] authorize];
        [self.navigationItem.rightBarButtonItem setTitle:NSLocalizedString(@"Log Out", nil)];
    }
}



#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"searchSegue"]) {
        [segue.destinationViewController setToken:_authToken];
    }
}


@end
