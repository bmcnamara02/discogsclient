//
//  MainViewController.h
//  DiscogsClient
//
//  Created by Barry McNamara on 12/07/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController <UIActionSheetDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) NSString *authToken;

@end
