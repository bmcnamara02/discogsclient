//
//  DiscogsAPIClient.h
//  DiscogsClient
//
//  Created by Barry McNamara on 12/07/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface DiscogsAPIClient : AFHTTPSessionManager

+ (DiscogsAPIClient *)sharedClientInitWithToken:(NSString *)token;

- (NSURLSessionDataTask *)searchForTerm:(NSString *)term
                             completion:( void (^)(NSArray *results, NSError *error) )completion;

- (NSURLSessionDataTask *)getWishlistCompletion:( void (^)(NSArray *results, NSError *error) )completion;


@end
