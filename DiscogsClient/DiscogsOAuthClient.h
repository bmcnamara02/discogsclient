//
//  DiscogsOAuthClient.h
//  DiscogsClient
//
//  Created by Barry McNamara on 13/07/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import <Foundation/Foundation.h>


NSString * const DiscogsClientErrorDomain;
NSString * const DiscogsClientDidLogInNotification;
NSString * const DiscogsClientDidLogOutNotification;

@interface DiscogsOAuthClient : NSObject

@property (nonatomic, assign, readonly, getter = isAuthorized) BOOL authorized;

#pragma mark Initialization
+ (instancetype)createWithConsumerKey:(NSString *)apiKey secret:(NSString *)secret;
+ (instancetype)sharedClient;

#pragma mark Authorization
- (BOOL)isAuthorized;
+ (BOOL)isAuthorizationCallbackURL:(NSURL *)url;
- (void)authorize;
- (BOOL)handleAuthorizationCallbackURL:(NSURL *)url;
- (void)deauthorize;


@end
