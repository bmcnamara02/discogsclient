//
//  DiscogsOAuthClient.m
//  DiscogsClient
//
//  Created by Barry McNamara on 13/07/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import "DiscogsOAuthClient.h"
#import "BDBOAuth1RequestOperationManager.h"
#import "BDBOAuth1SessionManager.h"
#import "NSDictionary+BDBOAuth1Manager.h"


// Exported
NSString * const DiscogsOAuthClientErrorDomain = @"DiscogsOAuthClientErrorDomain";

NSString * const DiscogsOAuthClientDidLogInNotification  = @"DiscogsOAuthClientDidLogInNotification";
NSString * const DiscogsOAuthClientDidLogOutNotification = @"DiscogsOAuthClientDidLogOutNotification";

// Internal
static NSString * const kDiscogsOAuthClientAPIURL   = @"http://api.discogs.com";

static NSString * const kDiscogsOAuthClientOAuthAuthorizeURL     = @"https://discogs.com/oauth/authorize";
static NSString * const kDiscogsOAuthClientOAuthCallbackURL      = @"bdboauth1demo-discogs://authorize";
static NSString * const kDiscogsOAuthClientOAuthRequestTokenPath = @"/oauth/request_token";
static NSString * const kDiscogsOAuthClientOAuthAccessTokenPath  = @"/oauth/access_token";

#pragma mark -
@interface DiscogsOAuthClient ()

@property (nonatomic) BDBOAuth1SessionManager *networkManager;
- (id)initWithConsumerKey:(NSString *)key sceret:(NSString *)secret;

@end

@implementation DiscogsOAuthClient

#pragma mark Initialization
static DiscogsOAuthClient *_sharedClient = nil;

+ (instancetype)createWithConsumerKey:(NSString *)key secret:(NSString *)secret {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[[self class] alloc] initWithConsumerKey:key sceret:secret];
    });
    
    return _sharedClient;
}

- (id)initWithConsumerKey:(NSString *)key sceret:(NSString *)secret {
    self = [super init];
    
    if (self) {
        NSURL *baseURL = [NSURL URLWithString:kDiscogsOAuthClientAPIURL];
        _networkManager = [[BDBOAuth1SessionManager alloc] initWithBaseURL:baseURL consumerKey:key consumerSecret:secret];
    }
    
    return self;
}

+ (instancetype)sharedClient {
    NSAssert(_sharedClient, @"BDBTwitterClient not initialized. [BDBTwitterClient createWithConsumerKey:secret:] must be called first.");
    
    return _sharedClient;
}

#pragma mark Authorization
+ (BOOL)isAuthorizationCallbackURL:(NSURL *)url {
    NSURL *callbackURL = [NSURL URLWithString:kDiscogsOAuthClientOAuthCallbackURL];
    
    return _sharedClient && [url.scheme isEqualToString:callbackURL.scheme] && [url.host isEqualToString:callbackURL.host];
}

- (BOOL)isAuthorized {
    return self.networkManager.authorized;
}

- (void)authorize {
    [self.networkManager fetchRequestTokenWithPath:kDiscogsOAuthClientOAuthRequestTokenPath
                                            method:@"POST"
                                       callbackURL:[NSURL URLWithString:kDiscogsOAuthClientOAuthCallbackURL]
                                             scope:nil
                                           success:^(BDBOAuth1Credential *requestToken) {
                                               // Perform Authorization via MobileSafari
                                               NSString *authURLString = [kDiscogsOAuthClientOAuthAuthorizeURL stringByAppendingFormat:@"?oauth_token=%@", requestToken.token];
                                               NSLog(@"AuthURL: %@", authURLString);
                                               [[UIApplication sharedApplication] openURL:[NSURL URLWithString:authURLString]];
                                           }
                                           failure:^(NSError *error) {
                                               NSLog(@"Error: %@", error.localizedDescription);
                                               
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                               message:NSLocalizedString(@"Could not acquire OAuth request token. Please try again later.", nil)
                                                                              delegate:self
                                                                     cancelButtonTitle:NSLocalizedString(@"Dismiss", nil)
                                                                     otherButtonTitles:nil] show];
                                               });
                                           }];
}

- (BOOL)handleAuthorizationCallbackURL:(NSURL *)url {
    NSDictionary *parameters = [NSDictionary bdb_dictionaryFromQueryString:url.query];
    
    if (parameters[BDBOAuth1OAuthTokenParameter] && parameters[BDBOAuth1OAuthVerifierParameter]) {
        [self.networkManager fetchAccessTokenWithPath:kDiscogsOAuthClientOAuthAccessTokenPath
                                               method:@"POST"
                                         requestToken:[BDBOAuth1Credential credentialWithQueryString:url.query]
                                              success:^(BDBOAuth1Credential *accessToken) {
                                                  [[NSNotificationCenter defaultCenter] postNotificationName:DiscogsClientDidLogInNotification
                                                                                                      object:self
                                                                                                    userInfo:accessToken.userInfo];
                                              }
                                              failure:^(NSError *error) {
                                                  NSLog(@"Error: %@", error.localizedDescription);
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                                  message:NSLocalizedString(@"Could not acquire OAuth access token. Please try again later.", nil)
                                                                                 delegate:self
                                                                        cancelButtonTitle:NSLocalizedString(@"Dismiss", nil)
                                                                        otherButtonTitles:nil] show];
                                                  });
                                              }];
        
        return YES;
    }
    
    return NO;
}

- (void)deauthorize {
    [self.networkManager deauthorize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:DiscogsClientDidLogOutNotification object:self];
}

@end
